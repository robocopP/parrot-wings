using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace ParrotW.iOS
{
    public partial class InsetTextField : UITextField
    {
        public InsetTextField (IntPtr handle) : base (handle)
        {
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            //UIView* paddingView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, 5, 20)];
            //textField.leftView = paddingView;
            //textField.leftViewMode = UITextFieldViewModeAlways;

            UIView padding = new UIView(new CGRect(0, 0, 5, 20));
            this.LeftView = padding;
            this.LeftViewMode = UITextFieldViewMode.Always;
        }
    }
}