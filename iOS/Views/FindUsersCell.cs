using Foundation;
using System;
using UIKit;

namespace ParrotW.iOS
{
    public partial class FindUsersCell : UITableViewCell
    {
        public FindUsersCell (IntPtr handle) : base (handle)
        {
        }

        public void SetData(FilterUser f)
        {
            if (labelFindUserName != null)
                labelFindUserName.Text = f.name;

        }
    }
}