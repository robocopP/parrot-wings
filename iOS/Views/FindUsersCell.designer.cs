// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ParrotW.iOS
{
    [Register ("FindUsersCell")]
    partial class FindUsersCell
    {
        [Outlet]
        UIKit.UILabel labelFindUserName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (labelFindUserName != null) {
                labelFindUserName.Dispose ();
                labelFindUserName = null;
            }
        }
    }
}