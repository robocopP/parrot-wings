// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ParrotW.iOS
{
    [Register ("TransactionCell")]
    partial class TransactionCell
    {
        [Outlet]
        UIKit.UILabel labelSendAmountCredit { get; set; }


        [Outlet]
        UIKit.UILabel labelSendAmountDebit { get; set; }


        [Outlet]
        UIKit.UILabel labelSendTime { get; set; }


        [Outlet]
        UIKit.UILabel labelUserBalance { get; set; }


        [Outlet]
        UIKit.UILabel labelUserDidSended { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (labelSendAmountCredit != null) {
                labelSendAmountCredit.Dispose ();
                labelSendAmountCredit = null;
            }

            if (labelSendAmountDebit != null) {
                labelSendAmountDebit.Dispose ();
                labelSendAmountDebit = null;
            }

            if (labelSendTime != null) {
                labelSendTime.Dispose ();
                labelSendTime = null;
            }

            if (labelUserBalance != null) {
                labelUserBalance.Dispose ();
                labelUserBalance = null;
            }

            if (labelUserDidSended != null) {
                labelUserDidSended.Dispose ();
                labelUserDidSended = null;
            }
        }
    }
}