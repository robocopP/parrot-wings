﻿using System;
using UIKit;
using Foundation;
using CoreAnimation;

namespace ParrotW.iOS.Views
{


    public partial class GradientLayer: CAGradientLayer
    {
        public GradientLayer()
        { 
            this.Colors = new[] { UIColor.White.CGColor, UIColor.Clear.FromHex(0xE0E0E0).CGColor};
      //      this.StartPoint = new CoreGraphics.CGPoint(0.5, 1);
      //      this.EndPoint = new CoreGraphics.CGPoint(0.5, 0)
            this.Locations = new NSNumber[] { 0, 1 };

          //  this.BackgroundColor = UIColor.Clear.CGColor;
        }
    }
}
