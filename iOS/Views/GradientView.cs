using System;
using Foundation;
using UIKit;
using CoreAnimation;
using ObjCRuntime;

namespace ParrotW.iOS
{
    public partial class GradientView : UIView
    {
        public GradientView (IntPtr handle) : base (handle)
        {
        }

        [Export("layerClass")]
        public static Class LayerClass()
        {
            return new Class(typeof(Views.GradientLayer));
        }
    }
}