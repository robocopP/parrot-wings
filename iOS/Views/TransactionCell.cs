using Foundation;
using System;
using UIKit;
using System.Collections.Generic;

namespace ParrotW.iOS
{
    public partial class TransactionCell : UITableViewCell
    {
        public TransactionCell (IntPtr handle) : base (handle)
        {
        }

        public void SetData(Transaction t)
        {
            if (labelUserDidSended != null)
            labelUserDidSended.Text = t.name;

            if (labelSendTime != null) 
            labelSendTime.Text = t.date;

            if (labelSendAmountDebit != null)
            labelSendAmountDebit.Text = t.amount.ToString();

            if (labelSendAmountCredit != null)
            labelSendAmountCredit.Text = t.amount.ToString();

            if (labelUserBalance != null)
                labelUserBalance.Text = "Баланс: " + t.balance.ToString();
        }
    }
}