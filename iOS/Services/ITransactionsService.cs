﻿using System;
using Refit;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ParrotW.iOS.Services
{
    [Headers("Authorization: Bearer")]
    public interface ITransactionsService
    {
        [Get("/api/protected/transactions")]
        Task<TransactionsL> readTransactions([Header("Authorization")] string token);
        Task<TransactionsL> readTransactions([Header("Authorization")] string token, SuccessBlock scallback, FailureBlock fcallback);

        [Post("/api/protected/transactions")]
        Task<TransactionD> createTransaction([Header("Authorization")] string token, [Body] Dictionary<string, string> body);
        Task<TransactionD> createTransaction([Header("Authorization")] string token, [Body] Dictionary<string, string> body, SuccessBlock scallback, FailureBlock fcallback);
    }
}
