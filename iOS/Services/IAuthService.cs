﻿using System;
using Refit;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ParrotW.iOS.Services
{
    public interface IAuthService
    {
        [Post("/users")]
        Task<UserCredentials> register([Body] Dictionary<string, string> body);
        Task<UserCredentials> register([Body] Dictionary<string, string> body, SuccessBlock scallback, FailureBlock fcallback);

        [Post("/sessions/create")]
        Task<UserCredentials> authenticate([Body] Dictionary<string, string> body);
        Task<UserCredentials> authenticate([Body] Dictionary<string, string> body, SuccessBlock scallback, FailureBlock fcallback);
    }
}
