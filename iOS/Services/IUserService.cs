﻿using System;
using Refit;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ParrotW.iOS.Services
{
    public interface IUserService
    {
        [Get("/api/protected/user-info")]
        Task<UserInfo> readUserInfo([Header("Authorization")] string token);
        Task<UserInfo> readUserInfo([Header("Authorization")] string token, SuccessBlock scallback, FailureBlock fcallback);

        Task<FilterUser> filterUsersList(string token, Dictionary<string, string> filter, SuccessBlock scallback, FailureBlock fcallback);
    }
}
