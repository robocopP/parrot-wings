﻿using Refit;
using System.Net.Http;
using System.Threading.Tasks;
using Akavache;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;
using System.Net;
using System.Net.Mime;
using System.Net.Http.Headers;

namespace ParrotW.iOS.Services
{
    public class UserService
    {
        private readonly IUserService _theUserApi;

        public UserService(string baseUrl)
        {
            _theUserApi = RestService.For<IUserService>(baseUrl);
        }

        async public void readUserInfo(string token, SuccessBlock scallback, FailureBlock fcallback)
        {
            await _theUserApi.readUserInfo(token)
                .ContinueWith(result =>
                {
                    if (result.Status != System.Threading.Tasks.TaskStatus.Faulted)
                    {
                        var userInfo = result.Result;
                        if (userInfo != null)
                        {
                            BlobCache.UserAccount.InsertObject(Strings.UserInfoIdentifier, userInfo);
                            scallback();

                        }
                        else
                        {
                            fcallback();
                        }
                    } else
                    {
                        fcallback();
                    }


                }).ConfigureAwait(false);
        }

        public async void filterUsersList(string token, Dictionary<string, string> filter, SuccessBlock scallback, FailureBlock fcallback)
        {
            using (var client = new HttpClient())
            {
                var url = string.Format(Strings.baseURLString + "/api/protected/users/list");
                var package = JsonConvert.SerializeObject(filter);

                HttpContent content = new StringContent(package, Encoding.UTF8, MediaTypeNames.Application.Json);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                await client.PostAsync(url, content).ContinueWith(result => {
                    if (result.Status != System.Threading.Tasks.TaskStatus.Faulted)
                    {
                        var jsonStr = result.Result.Content.ReadAsStringAsync();
                        var usersList = JsonConvert.DeserializeObject<List<FilterUser>>(jsonStr.Result);

                        if (usersList != null)
                        {
                            BlobCache.UserAccount.InsertObject(Strings.FilteredUsersIdentifier, usersList);
                            scallback();

                        }
                        else
                        {
                            fcallback();
                        }

                    } else
                    {
                        fcallback();
                    }

                }).ConfigureAwait(false);


            }
        }

        //async public void filterUsersList(string token, Dictionary<string, string> filter, SuccessBlock scallback, FailureBlock fcallback)
        //{
        //    await _theUserApi.filterUsersList(token, filter)
        //        .ContinueWith(result =>
        //        {
        //            var usersList = result.Result;
        //            if (usersList != null)
        //            {
        //                BlobCache.UserAccount.InsertObject(Strings.FilteredUsersIdentifier, usersList);
        //                scallback();

        //            }
        //            else
        //            {
        //                fcallback();
        //            }

        //        }).ConfigureAwait(false);
        //}
    }
}
