﻿using Refit;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Akavache;

namespace ParrotW.iOS.Services
{
    public class TransactionsService
    {
        private readonly ITransactionsService _theTransactionsApi;

        public TransactionsService(string baseUrl)
        {
            _theTransactionsApi = RestService.For<ITransactionsService>(baseUrl);
        }

        async public void readTransactions(string token, SuccessBlock scallback, FailureBlock fcallback)
        {
            await _theTransactionsApi.readTransactions(token)
                .ContinueWith(result =>
                {
                    if (result.Status != System.Threading.Tasks.TaskStatus.Faulted)
                    {
                        var credentials = result.Result;
                        if (credentials != null)
                        {
                            BlobCache.UserAccount.InsertObject(Strings.TransactionsIdentifier, credentials);
                            scallback();

                        }
                        else
                        {
                            fcallback();
                        }
                    } else
                    {
                        fcallback();
                    }

                }).ConfigureAwait(false);
        }

        async public void createTransaction(string token, Dictionary<string, string> body, SuccessBlock scallback, FailureBlock fcallback)
        {
            await _theTransactionsApi.createTransaction(token, body)
                .ContinueWith(result =>
                {
                    if (result.Status != System.Threading.Tasks.TaskStatus.Faulted)
                    {
                        var credentials = result.Result;
                        if (credentials != null)
                        {
                            scallback();

                        }
                        else
                        {
                            fcallback();
                        }
                    } else
                    {
                        fcallback();
                    }

                }).ConfigureAwait(false);
        }
    }
}
