﻿using Refit;
using System.Collections.Generic;
using System.Reactive.Linq;
using Akavache;

namespace ParrotW.iOS.Services
{
    public class AuthService
    {
        private readonly IAuthService _theUserApi;

        public AuthService(string baseUrl)
        {
            _theUserApi = RestService.For<IAuthService>(baseUrl);
        }

        async public void authenticate(Dictionary<string, string> body, SuccessBlock scallback, FailureBlock fcallback)
        {
            await _theUserApi.authenticate(body)
                .ContinueWith(result =>
                {
                    if (result.Status != System.Threading.Tasks.TaskStatus.Faulted)
                    {
                        var credentials = result.Result;
                        if (credentials != null)
                        {
                            BlobCache.UserAccount.InsertObject(Strings.CredentialsIdentifier, credentials);
                            scallback();

                        }
                        else
                        {
                            fcallback();
                        }
                    } else
                    {
                        fcallback();
                    }

                }).ConfigureAwait(false);
        }

        async public void register(Dictionary<string, string> body, SuccessBlock scallback, FailureBlock fcallback)
        {
            await _theUserApi.register(body)
                .ContinueWith(result =>
                {
                    if (result.Status != System.Threading.Tasks.TaskStatus.Faulted)
                    {
                        var credentials = result.Result;
                        if (credentials != null)
                        {
                            scallback();

                        }
                        else
                        {
                            fcallback();
                        }
                    } else
                    {
                        fcallback();
                    }

                }).ConfigureAwait(false);
        }

        //public async Task<UserCredentials> register(string username, string password, string email)
        //{
        //    return await _theUserApi.register(username, password, email).ConfigureAwait(false);
        //}
    }
}
