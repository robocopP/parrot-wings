using System;
using ParrotW.iOS.Services;
using UIKit;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Refit;
using BigTed;
using Akavache;
using Foundation;

namespace ParrotW.iOS
{
    public partial class PWAmountController : UIViewController
    {
        public PWAmountController (IntPtr handle) : base (handle)
        {
        }

        public void SetUserNameToBeSend(string userNameToBeSend)
        {
            _userNameToBeSend = userNameToBeSend;
        }

        private UserInfo info;
        private string _userNameToBeSend;
        private UserCredentials credentials;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            BlobCache.UserAccount.GetObject<UserInfo>(Strings.UserInfoIdentifier)
                .Subscribe(x => {
                    UIApplication.SharedApplication.InvokeOnMainThread(delegate
                    {
                        this.info = x;
                    });
                });

            BlobCache.UserAccount.GetObject<UserCredentials>(Strings.CredentialsIdentifier)
                .Subscribe(x => {
                    UIApplication.SharedApplication.InvokeOnMainThread(delegate
                    {
                        this.credentials = x;
                    });
                });

            buttonSubmit.AddTarget(CheckInput, UIControlEvent.TouchUpInside);

            this.Title = "Введите PW";
        }

        public bool IsInputRight()
        {
            return (textFieldAmount.Text.Length > 0 && Convert.ToInt32(textFieldAmount.Text) > 0 && _userNameToBeSend != null);
        }

        public bool IsEnoughMoney()
        {
            return (Convert.ToInt32(textFieldAmount.Text) <= info.user.balance);
        }

        public void CheckInput(object sender, EventArgs args)
        {
            if (IsInputRight())
            {
                if (IsEnoughMoney())
                {
                    Dictionary<string, string> authData = new Dictionary<string, string>
                {
                {"name", _userNameToBeSend},
    {"amount", textFieldAmount.Text}
                };

                    var transactionsService = new TransactionsService(Strings.baseURLString);

                    SuccessBlock a1 = ASuccessBlock;
                    FailureBlock a2 = AFailureBlock;

                    BTProgressHUD.Show();
                    transactionsService.createTransaction("Bearer " + credentials.token, authData, a1, a2);

                } else
                {
                    var okCancelAlertController = UIAlertController.Create("У вас недостаточно средств на счетe", "", UIAlertControllerStyle.Alert);
                    okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => Console.WriteLine("Okay was clicked")));
                    PresentViewController(okCancelAlertController, true, null);
                }
            }
        }

        int loadCount = 0;
        void AllLoaded()
        {
            if (loadCount < 2)
            {
                return;
            }
            loadCount = 0;

            BTProgressHUD.Dismiss();
            UIApplication.SharedApplication.InvokeOnMainThread(delegate
            {
                NSNotificationCenter.DefaultCenter.PostNotificationName(Strings.UserInfoObserver, null);
                NSNotificationCenter.DefaultCenter.PostNotificationName(Strings.UserTransactionsObserver, null);

                UIApplication.SharedApplication.InvokeOnMainThread(delegate
                {
                    this.TabBarController.SelectedIndex = 0;
                    this.NavigationController.PopToRootViewController(true);

                });
            });
        }

        void ASuccessBlock()
        {
            SuccessBlock u1 = USuccessBlock;
            FailureBlock u2 = UFailureBlock;
            SuccessBlock t1 = TSuccessBlock;
            FailureBlock t2 = TFailureBlock;

            var userService = new UserService(Strings.baseURLString);
            var transactionsService = new TransactionsService(Strings.baseURLString);

            userService.readUserInfo("Bearer " + credentials.token, u1, u2);
            transactionsService.readTransactions("Bearer "+ credentials.token, t1, t2);

        }

        void AFailureBlock()
        {
            BTProgressHUD.Dismiss();
        }

        void USuccessBlock()
        {
            loadCount++;
            AllLoaded();
        }

        void UFailureBlock()
        {
            BTProgressHUD.Dismiss();
        }

        void TSuccessBlock()
        {
            loadCount++;
            AllLoaded();
        }

        void TFailureBlock()
        {
            BTProgressHUD.Dismiss();
        }
    }
}