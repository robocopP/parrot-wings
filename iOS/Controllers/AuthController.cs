using System;
using ParrotW.iOS.Services;
using UIKit;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Refit;
using BigTed;
using Akavache;
using Foundation;

namespace ParrotW.iOS
{
    public partial class AuthController : UIViewController
    {
        public AuthController (IntPtr handle) : base (handle)
        {
        }

        public bool IsInputRight()
        {
            if (textFieldEmail.Text.Length > 0 && textFieldPass.Text.Length > 0 && Helper.IsValidEmail(textFieldEmail.Text))
            {
                return true;
            }

            return false;
        }

        public void CheckInput(object sender, EventArgs args)
        {
            if (IsInputRight())
            {
                Dictionary<string, string> authData = new Dictionary<string, string>
                {
                {"email", textFieldEmail.Text},
    {"password", textFieldPass.Text }
                };

                var authService = new AuthService(Strings.baseURLString);

                SuccessBlock a1 = ASuccessBlock;
                FailureBlock a2 = AFailureBlock;

                BTProgressHUD.Show();
                authService.authenticate(authData, a1, a2);

            } else
            {
                UIApplication.SharedApplication.InvokeOnMainThread(delegate
                {
                    var okCancelAlertController = UIAlertController.Create("Проверка данных не прошла валидацию", "Вы ввели некорректный e-mail, либо забыли ввести данные в одно из двух полей. Пожалуйста, попробуйте еще раз.", UIAlertControllerStyle.Alert);
                    okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => Console.WriteLine("Okay was clicked")));
                    PresentViewController(okCancelAlertController, true, null);
                });
            }
        }

        public void ToRegistration(object sender, EventArgs args)
        {
            var storyboard = UIStoryboard.FromName("Main", null);
            RegistrationController controller = (RegistrationController) storyboard.InstantiateViewController("RegistrationStory");
            controller.aController = this;

            this.NavigationController.PushViewController(controller, true);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            buttonSubmit.AddTarget(CheckInput, UIControlEvent.TouchUpInside);
            buttonRegistrate.AddTarget(ToRegistration, UIControlEvent.TouchUpInside);
        }

        public void UpdateAuthInfo(Models.AuthInfo a)
        {
            textFieldEmail.Text = a.email;
            textFieldPass.Text = a.pass;
        }

        int loadCount = 0;
        void AllLoaded()
        {
            if (loadCount < 3) {
                return;
            }
            loadCount = 0;

            BTProgressHUD.Dismiss();
            UIApplication.SharedApplication.InvokeOnMainThread(delegate
            {
                var storyboard = UIStoryboard.FromName("Main", null);
                UIViewController controller = storyboard.InstantiateViewController("TabBarStory");
                this.PresentModalViewController(controller, true);
            });
        }

        void ASuccessBlock()
        {
            var userService = new UserService(Strings.baseURLString);
            var transactionsService = new TransactionsService(Strings.baseURLString);

            SuccessBlock u1 = USuccessBlock;
            FailureBlock u2 = UFailureBlock;
            SuccessBlock t1 = TSuccessBlock;
            FailureBlock t2 = TFailureBlock;
            SuccessBlock f1 = FSuccessBlock;
            FailureBlock f2 = FFailureBlock;

            Dictionary<string, string> filterData = new Dictionary<string, string>
                {
                {"filter", "a"}
                };

            UserCredentials credentials;
            BlobCache.UserAccount.GetObject<UserCredentials>(Strings.CredentialsIdentifier)
                .Subscribe(x => {
                    credentials = x; 
                    userService.readUserInfo("Bearer "+credentials.token, u1, u2);
                    transactionsService.readTransactions("Bearer "+credentials.token, t1, t2);
                    userService.filterUsersList(credentials.token, filterData, f1, f2);
                });
                    
        }

        void AFailureBlock()
        {
            BTProgressHUD.Dismiss();

            UIApplication.SharedApplication.InvokeOnMainThread(delegate
            {
                var okCancelAlertController = UIAlertController.Create("Не получилось авторизоваться", "Вероятнее всего, вы неправильно ввели e-mail или пароль. Пожалуйста, уточните эти данные и попробуйте нажать еще раз.", UIAlertControllerStyle.Alert);
                okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => Console.WriteLine("Okay was clicked")));
                PresentViewController(okCancelAlertController, true, null);
            });
        }

        void USuccessBlock()
        {
            loadCount++;
            AllLoaded();
        }

        void UFailureBlock()
        {
            BTProgressHUD.Dismiss();
        }

        void TSuccessBlock()
        {
            loadCount++;
            AllLoaded();
        }

        void TFailureBlock()
        {
            BTProgressHUD.Dismiss();
        }

        void FSuccessBlock()
        {
            loadCount++;
            AllLoaded();
        }

        void FFailureBlock()
        {
            BTProgressHUD.Dismiss();
        }
    }
}