// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ParrotW.iOS
{
    [Register ("ProfileController")]
    partial class ProfileController
    {
        [Outlet]
        UIKit.UIButton buttonSend { get; set; }


        [Outlet]
        UIKit.UILabel labelUserAmount { get; set; }


        [Outlet]
        UIKit.UILabel labelUserEmail { get; set; }


        [Outlet]
        UIKit.UILabel labelUserName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (buttonSend != null) {
                buttonSend.Dispose ();
                buttonSend = null;
            }

            if (labelUserAmount != null) {
                labelUserAmount.Dispose ();
                labelUserAmount = null;
            }

            if (labelUserEmail != null) {
                labelUserEmail.Dispose ();
                labelUserEmail = null;
            }

            if (labelUserName != null) {
                labelUserName.Dispose ();
                labelUserName = null;
            }
        }
    }
}