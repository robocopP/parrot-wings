using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using BigTed;
using Akavache;

namespace ParrotW.iOS
{
    public partial class RegistrationController : UIViewController
    {
        public RegistrationController (IntPtr handle) : base (handle)
        {
        }

        public AuthController aController;

        public bool IsInputRight()
        {
            if (textFieldEmail.Text.Length > 0 && textFieldPass.Text.Length > 0 && textFieldName.Text.Length > 0 && Helper.IsValidEmail(textFieldEmail.Text))
            {
                return true;
            }

            return false;
        }

        public void CheckInput(object sender, EventArgs args)
        {
            if (IsInputRight())
            {

                Dictionary<string, string> regData = new Dictionary<string, string>
                {
                {"email", textFieldEmail.Text},
    {"password", textFieldPass.Text },
                    {"username", textFieldName.Text}
                };

                var authService = new Services.AuthService(Strings.baseURLString);

                SuccessBlock a1 = SuccessBlock;
                FailureBlock a2 = FailureBlock;

                BTProgressHUD.Show();
                authService.register(regData, a1, a2);
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            buttonSubmit.AddTarget(CheckInput, UIControlEvent.TouchUpInside);

        }

        void SuccessBlock()
        {
            UIApplication.SharedApplication.InvokeOnMainThread(delegate
            {
                BTProgressHUD.Dismiss();
                Models.AuthInfo a = new Models.AuthInfo(textFieldEmail.Text, textFieldPass.Text);
                aController.UpdateAuthInfo(a);

                this.NavigationController.PopViewController(true);
            });
        }

        void FailureBlock()
        {
            BTProgressHUD.Dismiss();

            UIApplication.SharedApplication.InvokeOnMainThread(delegate
            {
                var okCancelAlertController = UIAlertController.Create("Не получилось зарегистрироваться", "Скорее всего, этот e-mail уже занят. Попробуйте ввести другой.", UIAlertControllerStyle.Alert);
                okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => Console.WriteLine("Okay was clicked")));
                PresentViewController(okCancelAlertController, true, null);
            });
        }
    }
}