// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ParrotW.iOS
{
    [Register ("TransactionsController")]
    partial class TransactionsController
    {
        [Outlet]
        UIKit.UITableView transactionsTableView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (transactionsTableView != null) {
                transactionsTableView.Dispose ();
                transactionsTableView = null;
            }
        }
    }
}