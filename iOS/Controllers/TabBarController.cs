using System;
using Foundation;
using UIKit;
using CoreAnimation;
using CoreGraphics;

namespace ParrotW.iOS
{
    public partial class TabBarController : UITabBarController
    {
        public TabBarController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            this.ViewControllers[0].TabBarItem.Title = "Профиль";
            this.ViewControllers[1].TabBarItem.Title = "Транзакции";


            //    UIImage *quiz = [UIImage imageWithIcon:@"fa-list" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] fontSize:24];
            //    UIImage *appeal = [UIImage imageWithIcon:@"fa-envelope" backgroundColor:[UIColor clearColor] iconColor:[UIColor whiteColor] fontSize:24];


            this.ViewControllers[0].TabBarItem.Image = UIImage.FromBundle("profile").ResizeUIImage(28, 28);
            this.ViewControllers[1].TabBarItem.Image = UIImage.FromBundle("pw").ResizeUIImage(24, 24);

            this.ViewControllers[0].TabBarItem.SelectedImage = UIImage.FromBundle("profile.png").ResizeUIImage(28, 28);
            this.ViewControllers[1].TabBarItem.SelectedImage = UIImage.FromBundle("pw.png").ResizeUIImage(24, 24);
            //    self.viewControllers[0].tabBarItem.image = [UIImage imageWithIcon: @"fa-list" backgroundColor:[UIColor clearColor] iconColor:[[UIColor whiteColor] colorWithAlphaComponent: 0.4] fontSize: 24];
            //    self.viewControllers[1].tabBarItem.image = [UIImage imageWithIcon: @"fa-envelope" backgroundColor:[UIColor clearColor] iconColor:[[UIColor whiteColor] colorWithAlphaComponent: 0.4] fontSize: 24];


            //    self.viewControllers[0].tabBarItem.selectedImage = [UIImage imageWithIcon: @"fa-list" backgroundColor:[UIColor clearColor] iconColor:[[UIColor whiteColor] colorWithAlphaComponent: 1] fontSize: 24];
            //    self.viewControllers[1].tabBarItem.selectedImage = [UIImage imageWithIcon: @"fa-envelope" backgroundColor:[UIColor clearColor] iconColor:[[UIColor whiteColor] colorWithAlphaComponent: 1] fontSize: 24];


            //    if (iOSVersionLessThan(@"10.0"))
            //    {
            //for(UITabBarItem* item in self.tabBarController.tabBar.items)
            //    {
            //        item.image = [item.image imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
            //    }

            //}
        }
    }
}