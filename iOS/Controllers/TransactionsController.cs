using Foundation;
using System;
using UIKit;
using Akavache;
using System.Collections.Generic;
using BigTed;

namespace ParrotW.iOS
{
    public partial class TransactionsController : UIViewController
    {
        public TransactionsController (IntPtr handle) : base (handle)
        {
        }

        public List<Transaction> transactions;
        public UserInfo info;
        public UserCredentials credentials;

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.Title = "Транзакции";

            UpdateTransactions();

            NSNotificationCenter.DefaultCenter.AddObserver(new NSString(Strings.UserTransactionsObserver), TransactionsDidUpdated);
        }

        void TransactionsDidUpdated(NSNotification notification)
        {
            UpdateTransactions();
        }

        void UpdateTransactions()
        {
            BlobCache.UserAccount.GetObject<TransactionsL>(Strings.TransactionsIdentifier)
                .Subscribe(x =>
                {
                    this.transactions = x.transactions;
                    UIApplication.SharedApplication.InvokeOnMainThread(delegate
                    {
                        transactionsTableView.Source = new Source(this);
                        transactionsTableView.Delegate = new Delegate(this);
                        this.transactionsTableView.ReloadData();
                    });
                });

            BlobCache.UserAccount.GetObject<UserInfo>(Strings.UserInfoIdentifier)
                .Subscribe(x =>
                {
                    this.info = x;
                });

            BlobCache.UserAccount.GetObject<UserCredentials>(Strings.CredentialsIdentifier)
                .Subscribe(x =>
                {
                    this.credentials = x;
                });

        }

        private class Delegate: UITableViewDelegate
        {
            private TransactionsController _myViewController;

            public Delegate(TransactionsController myViewController)
            {
                _myViewController = myViewController;
            }

            private string tempName;
            private string tempAmount;

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {

                tempName = _myViewController.transactions[indexPath.Row].name;
                tempAmount = _myViewController.transactions[indexPath.Row].amount.ToString();

                UIApplication.SharedApplication.InvokeOnMainThread(delegate
                {
                    var okCancelAlertController = UIAlertController.Create(String.Format("Вы точно хотите отправить валюту (PW) в размере {0} пользователю {1} еще раз?", System.Math.Abs(Convert.ToInt32(tempAmount)).ToString(), tempName), "", UIAlertControllerStyle.Alert);

                    okCancelAlertController.AddAction(UIAlertAction.Create("Нет", UIAlertActionStyle.Default, alert => Console.WriteLine("Cancel was clicked")));
                    okCancelAlertController.AddAction(UIAlertAction.Create("Да", UIAlertActionStyle.Cancel, alert => {
                        CheckInput();
                    }));

                    _myViewController.PresentViewController(okCancelAlertController, true, null);
                });

                
            }

            public bool IsEnoughMoney()
            {
                return (System.Math.Abs(Convert.ToInt32(tempAmount)) <= _myViewController.info.user.balance);
            }

            public void CheckInput()
            {
                    if (IsEnoughMoney())
                    {
                        Dictionary<string, string> authData = new Dictionary<string, string>
                {
                {"name", tempName},
    {"amount", System.Math.Abs(Convert.ToInt32(tempAmount)).ToString()}
                };

                        var transactionsService = new Services.TransactionsService(Strings.baseURLString);

                        SuccessBlock a1 = ASuccessBlock;
                        FailureBlock a2 = AFailureBlock;

                        BTProgressHUD.Show();
                        transactionsService.createTransaction("Bearer " + _myViewController.credentials.token, authData, a1, a2);

                    }
                    else
                    {
                        var okCancelAlertController = UIAlertController.Create("У вас недостаточно средств на счетe", "", UIAlertControllerStyle.Alert);
                        okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => Console.WriteLine("Okay was clicked")));
                       _myViewController.PresentViewController(okCancelAlertController, true, null);
                    }
            }

            int loadCount = 0;
            void AllLoaded()
            {
                if (loadCount < 2)
                {
                    return;
                }
                loadCount = 0;

                BTProgressHUD.Dismiss();
                UIApplication.SharedApplication.InvokeOnMainThread(delegate
                {
                    NSNotificationCenter.DefaultCenter.PostNotificationName(Strings.UserInfoObserver, null);
                    NSNotificationCenter.DefaultCenter.PostNotificationName(Strings.UserTransactionsObserver, null);

                    _myViewController.TabBarController.SelectedIndex = 0;
                });
            }

            void ASuccessBlock()
            {
                SuccessBlock u1 = USuccessBlock;
                FailureBlock u2 = UFailureBlock;
                SuccessBlock t1 = TSuccessBlock;
                FailureBlock t2 = TFailureBlock;

                var userService = new Services.UserService(Strings.baseURLString);
                var transactionsService = new Services.TransactionsService(Strings.baseURLString);

                userService.readUserInfo("Bearer " + _myViewController.credentials.token, u1, u2);
                transactionsService.readTransactions("Bearer " + _myViewController.credentials.token, t1, t2);

            }

            void AFailureBlock()
            {
                BTProgressHUD.Dismiss();
            }

            void USuccessBlock()
            {
                loadCount++;
                AllLoaded();
            }

            void UFailureBlock()
            {
                BTProgressHUD.Dismiss();
            }

            void TSuccessBlock()
            {
                loadCount++;
                AllLoaded();
            }

            void TFailureBlock()
            {
                BTProgressHUD.Dismiss();
            }
        }

        private class Source : UITableViewSource
        {
            private TransactionsController _myViewController;

            public Source(TransactionsController myViewController)
            {
                _myViewController = myViewController;
                Console.WriteLine("");
            }

            public string GetIdentifierForIndexPath(NSIndexPath path)
            {
                Transaction t = _myViewController.transactions[path.Row];
                return (t.amount > 0) ? "DebitCell" : "CreditCell";
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                var identifier = GetIdentifierForIndexPath(indexPath);
                TransactionCell tcell = (TransactionCell) tableView.DequeueReusableCell(identifier, indexPath);
                tcell.SetData(_myViewController.transactions[indexPath.Row]);

                return tcell;
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            { 
                return _myViewController.transactions.Count;
            }

            public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
            {
                return new nfloat(66);
            }
        }
    }
}