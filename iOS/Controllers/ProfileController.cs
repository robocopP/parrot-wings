using Foundation;
using System;
using UIKit;
using Akavache;

namespace ParrotW.iOS
{
    public partial class ProfileController : UIViewController
    {
        public ProfileController (IntPtr handle) : base (handle)
        {
        }

        void Exit (object sender, EventArgs args)
        {
            NSNotificationCenter.DefaultCenter.RemoveObserver(NSObject.FromObject(Strings.UserInfoObserver));
            NSNotificationCenter.DefaultCenter.RemoveObserver(NSObject.FromObject(Strings.UserTransactionsObserver));

            this.DismissViewController(true, null);
        }

        UserInfo tempUserInfo;
        private bool CheckEnoughMoney ()
        {
            return (tempUserInfo.user.balance > 0);
        }

        public void CheckInput(object sender, EventArgs args)
        {
            if (CheckEnoughMoney())
            {
                var storyboard = UIStoryboard.FromName("Main", null);
                UIViewController controller = storyboard.InstantiateViewController("FindUserControllerStory");
                this.NavigationController.PushViewController(controller, true);

            } else
            {
                var okCancelAlertController = UIAlertController.Create("У вас недостаточно денег на счете", "", UIAlertControllerStyle.Alert);
                okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => Console.WriteLine("Okay was clicked")));
                PresentViewController(okCancelAlertController, true, null);
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            UpdateUserInfo();

            buttonSend.AddTarget(CheckInput, UIControlEvent.TouchUpInside);
            NSNotificationCenter.DefaultCenter.AddObserver(new NSString(Strings.UserInfoObserver), UserInfoDidUpdated);

            var b = new UIBarButtonItem("Exit", UIBarButtonItemStyle.Plain, Exit);
            this.NavigationItem.RightBarButtonItem = b;
        }

        void UserInfoDidUpdated (NSNotification notification)
        {
            UpdateUserInfo();

            UIApplication.SharedApplication.InvokeOnMainThread(delegate
            {
                var okCancelAlertController = UIAlertController.Create("Поздравляем!", "Деньги были успешно отправлены.", UIAlertControllerStyle.Alert);
                okCancelAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => this.NavigationController.PopToRootViewController(true)));
                PresentViewController(okCancelAlertController, true, null);
            });
        }

        void UpdateUserInfo()
        {
            BlobCache.UserAccount.GetObject<UserInfo>(Strings.UserInfoIdentifier)
                .Subscribe(x =>
                {
                    UIApplication.SharedApplication.InvokeOnMainThread(delegate
                    {
                        this.labelUserName.Text = x.user.name;
                        this.labelUserEmail.Text = x.user.email;
                        this.labelUserAmount.Text = x.user.balance.ToString();

                        this.Title = x.user.name;

                        tempUserInfo = x;
                    });
                });
        }
    }
}