using Foundation;
using System;
using UIKit;

namespace ParrotW.iOS
{
    public partial class InvisibleNavigationController : UINavigationController
    {
        public InvisibleNavigationController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
            this.NavigationBar.ShadowImage = new UIImage();
            this.NavigationBar.Translucent = true;
        }
    }
}