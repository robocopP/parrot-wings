// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ParrotW.iOS
{
    [Register ("RegistrationController")]
    partial class RegistrationController
    {
        [Outlet]
        UIKit.UIButton buttonSubmit { get; set; }

        [Outlet]
        UIKit.UITextField textFieldEmail { get; set; }

        [Outlet]
        UIKit.UITextField textFieldName { get; set; }

        [Outlet]
        UIKit.UITextField textFieldPass { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (buttonSubmit != null) {
                buttonSubmit.Dispose ();
                buttonSubmit = null;
            }

            if (textFieldEmail != null) {
                textFieldEmail.Dispose ();
                textFieldEmail = null;
            }

            if (textFieldName != null) {
                textFieldName.Dispose ();
                textFieldName = null;
            }

            if (textFieldPass != null) {
                textFieldPass.Dispose ();
                textFieldPass = null;
            }
        }
    }
}