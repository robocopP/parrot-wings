// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ParrotW.iOS
{
    [Register ("PWAmountController")]
    partial class PWAmountController
    {
        [Outlet]
        UIKit.UIButton buttonSubmit { get; set; }


        [Outlet]
        UIKit.UITextField textFieldAmount { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (buttonSubmit != null) {
                buttonSubmit.Dispose ();
                buttonSubmit = null;
            }

            if (textFieldAmount != null) {
                textFieldAmount.Dispose ();
                textFieldAmount = null;
            }
        }
    }
}