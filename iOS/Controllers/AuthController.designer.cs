// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace ParrotW.iOS
{
    [Register ("AuthController")]
    partial class AuthController
    {
        [Outlet]
        UIKit.UIButton buttonRegistrate { get; set; }


        [Outlet]
        UIKit.UIButton buttonSubmit { get; set; }


        [Outlet]
        UIKit.UITextField textFieldEmail { get; set; }


        [Outlet]
        UIKit.UITextField textFieldPass { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (buttonRegistrate != null) {
                buttonRegistrate.Dispose ();
                buttonRegistrate = null;
            }

            if (buttonSubmit != null) {
                buttonSubmit.Dispose ();
                buttonSubmit = null;
            }

            if (textFieldEmail != null) {
                textFieldEmail.Dispose ();
                textFieldEmail = null;
            }

            if (textFieldPass != null) {
                textFieldPass.Dispose ();
                textFieldPass = null;
            }
        }
    }
}