using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using Akavache;

namespace ParrotW.iOS
{
    public partial class FindUsersController : UIViewController
    {
        public FindUsersController (IntPtr handle) : base (handle)
        {

        }

        public List<FilterUser> filterUsers;
        public List<FilterUser> sortFilterUsers;
        private UISearchController search;



        public List<FilterUser> GetList()
        {
            if (sortFilterUsers != null && sortFilterUsers.Count > 0)
            {
                return sortFilterUsers;

            }
            else if (filterUsers != null && filterUsers.Count > 0)
            {
                return filterUsers;

            }

            return null;
        }

        public FilterUser GetItemForIndexPath(NSIndexPath indexPath)
        {
            if (sortFilterUsers != null && sortFilterUsers.Count > 0)
            {
                return sortFilterUsers[indexPath.Row];

            }
            else if (filterUsers != null && filterUsers.Count > 0)
            {
                return filterUsers[indexPath.Row];

            }

            return null;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            search = new UISearchController(searchResultsController: null);
            search.SearchResultsUpdater = new SearchUpdater(this);

            search.DimsBackgroundDuringPresentation = false;

            search.SearchBar.SizeToFit();
            search.SearchBar.Placeholder = "Поиск";

            tableViewFindUsers.TableHeaderView = null;
            tableViewFindUsers.ContentInset = new UIEdgeInsets(0, 0, 0, 0);

            this.View.AddSubview(search.SearchBar);

            DefinesPresentationContext = true;

            BlobCache.UserAccount.GetObject<List<FilterUser>>(Strings.FilteredUsersIdentifier)
                .Subscribe(x =>
                {
                    this.filterUsers = x;
                    UIApplication.SharedApplication.InvokeOnMainThread(delegate
                    {
                        this.tableViewFindUsers.Source = new Source(this);
                        this.tableViewFindUsers.Delegate = new Delegate(this);
                        this.tableViewFindUsers.ReloadData();
                    });
                });

            this.Title = "Выберите пользователя";
        }

        private class Delegate : UITableViewDelegate
        {
            private FindUsersController _findUsersController;

            public Delegate(FindUsersController findUsersController)
            {
                _findUsersController = findUsersController;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                var storyboard = UIStoryboard.FromName("Main", null);
                PWAmountController controller = (PWAmountController) storyboard.InstantiateViewController("PWAmountControllerStory");
                controller.SetUserNameToBeSend(_findUsersController.GetItemForIndexPath(indexPath).name);
                _findUsersController.NavigationController.PushViewController(controller, true);
            }
        }

        private class Source : UITableViewSource
        {
            private FindUsersController _findUsersController;

            public Source(FindUsersController myViewController)
            {
                _findUsersController = myViewController;
            }
 
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                var identifier = "FilterCell";
                FindUsersCell tcell = (FindUsersCell)tableView.DequeueReusableCell(identifier, indexPath);
                tcell.SetData(_findUsersController.GetItemForIndexPath(indexPath));

                return tcell;
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return _findUsersController.GetList().Count;
            }

            public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
            {
                return new nfloat(66);
            }
        }

        private class SearchUpdater: UISearchResultsUpdating
        {
            FindUsersController _findUsersController;
            public SearchUpdater (FindUsersController fUsers)
            {
                _findUsersController = fUsers;
            }

            public override void UpdateSearchResultsForSearchController(UISearchController searchController)
            {
                String searchText = searchController.SearchBar.Text;
                List<FilterUser> tempArray = new List<FilterUser>();
                if (searchText.Length > 0)
                {
                    foreach (var user in _findUsersController.filterUsers)
                    {
                        if (user.name.ToLower().Contains(searchText.ToLower()))
                            tempArray.Add(user);
                    }
                }

                _findUsersController.sortFilterUsers = tempArray;
                _findUsersController.tableViewFindUsers.ReloadData();
            }
        }
    }
}