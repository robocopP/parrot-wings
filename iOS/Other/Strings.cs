﻿using System;
namespace ParrotW.iOS
{
    public static class Strings
    {
        public const string baseURLString = "http://193.124.114.46:3001";

        public const string CredentialsIdentifier = "CredentialsIdentifier";
        public const string UserInfoIdentifier = "UserInfoIdentifier";
        public const string TransactionsIdentifier = "TransactionsIdentifier";
        public const string FilteredUsersIdentifier = "FilteredUsersIdentifier";
        public const string RegistrationInfo = "RegistrationInfo";

        public const string UserInfoObserver = "UserInfoObserver";
        public const string UserTransactionsObserver = "UserTransactionsObserver";
        public const string RegistrationInfoReceivedObserver = "RegistrationInfoReceivedObserver";
    }
}
