﻿using System;
using UIKit;
using CoreGraphics;

public static class UIColorExtensions
{
    public static UIColor FromHex(this UIColor color, int hexValue)
    {
        return UIColor.FromRGB(
            (((float)((hexValue & 0xFF0000) >> 16)) / 255.0f),
            (((float)((hexValue & 0xFF00) >> 8)) / 255.0f),
            (((float)(hexValue & 0xFF)) / 255.0f)
        );
    }
}

public static class UIImageExtensions
{
    public static UIImage ResizeUIImage(this UIImage sourceImage, float widthToScale, float heightToScale)
    {
        var sourceSize = sourceImage.Size;
        var maxResizeFactor = Math.Max(widthToScale / sourceSize.Width, heightToScale / sourceSize.Height);
        if (maxResizeFactor > 1) return sourceImage;
        var width = maxResizeFactor * sourceSize.Width;
        var height = maxResizeFactor * sourceSize.Height;
        UIGraphics.BeginImageContext(new CGSize(width, height));
        sourceImage.Draw(new CGRect(0, 0, width, height));
        var resultImage = UIGraphics.GetImageFromCurrentImageContext();
        UIGraphics.EndImageContext();
        return resultImage;
    }
}

namespace ParrotW.iOS
{ 

    public class Helper
    {
        public Helper()
        {
        }

        public static bool IsValidEmail(string email)
        {
            try
            {
                var mail = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
