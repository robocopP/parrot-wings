﻿using System;
using Newtonsoft.Json;
using System.Collections;

namespace ParrotW.iOS
{

    public class User
    {
        public int id_ { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public int balance { get; set; }
    }

    public class UserInfo
    {
        [JsonProperty("user_info_token")]
        public User user;
    }

    public class UserCredentials
    {
        [JsonProperty("id_token")]
        public string token;
    }
}
