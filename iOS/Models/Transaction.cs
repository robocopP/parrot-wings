﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace ParrotW.iOS
{
    public class Transaction
    {
        [JsonProperty("id")]
        public int id_ { get; set; }
        [JsonProperty("date")]
        public string date { get; set; }
        [JsonProperty("username")]
        public string name { get; set; }
        [JsonProperty("amount")]
        public int amount { get; set; }
        [JsonProperty("balance")]
        public uint balance { get; set; }
    }

    public class TransactionsL
    {
        [JsonProperty("trans_token")]
        public List<Transaction> transactions { get; set; }
    }

    public class TransactionD
    {
        [JsonProperty("trans_token")]
        public Transaction transactions { get; set; }
    }
}
