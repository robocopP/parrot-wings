﻿using System;
namespace ParrotW.iOS.Models
{
    public class AuthInfo
    {
        public AuthInfo (string em, string pa)
        {
            email = em;
            pass = pa;
        }

        public string email;
        public string pass;
    }
}
